package org.example;

import java.util.HashSet;
import java.util.Set;

public class Main {

    static String getSecondEntry(String val, String replaceString, String replacement) {
        String[] strings = val.split("(?i)" + replaceString);
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < strings.length; i++) {
            sb.append(strings[i]);

            if (i != strings.length - 1) {
                sb.append(i % 2 == 1 ? replacement : replaceString);
            }
        }

        return sb.toString();
    }

    static boolean isPolyndrom(String str) {
        char[] chars = str.toCharArray();

        if (chars.length == 0) return false;
        if (chars.length == 1) return true;

        for (int i = 0; i < chars.length / 2; i++) {
            if (chars[i] != chars[chars.length - 1 - i]){
                return false;
            }
        }

        return true;
    }

    static String findWordWithMinSymbols(String words) {
        String[] strings = words.split(" ");
        String minWordsString = null;
        int minWordsCount = Integer.MAX_VALUE;


        for (String string : strings) {
            Set<Character> chars = new HashSet<>();

            for (char c : string.toCharArray()) {
                chars.add(c);
            }

            if (minWordsCount > chars.size()) {
                minWordsCount = chars.size();
                minWordsString = string;
            }
        }

        return minWordsString;
    }

    public static void main(String[] args) {
        String www = "Object-oriented programming is a programming language model organized around objects rather than \"actions\" and data rather than logic. Object-oriented programming blabla. Object-oriented programming bla.";
        String h1 = "Object-oriented programming";
        String h2 = "OOP";

        System.out.println(getSecondEntry(www, h1, h2));
        System.out.println(findWordWithMinSymbols("fffff ab f 1234 jkjk"));

        String polyndrom = "дед баб топор";
        String[] array = polyndrom.split(" ");
        for (String item : array){
            if(isPolyndrom(item)){
                System.out.println(item);
            }
        };


    }
}